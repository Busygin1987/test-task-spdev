import React from 'react';
import styled from 'styled-components';

import { Typography } from 'components/Typography';
import Info from 'components/Info';
import Pagination from 'components/Pagination';

const Wrap = styled.div`
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    background-color: #9e9b9b;
    display: flex;
    align-items: center;
    flex-direction: column;
    padding: 24px;
`;

export default ({ 
    title,
    children,
    isFetching = null,
    error,
    setPagination = () => {},
    length,
    ...props
}) => (
    <Wrap>
        <Typography props={props}>
            <div style={{
                width: '300px',
                minHeight: '50px',
                textAlign: 'center'
            }}>
                {title}
            </div>
        </Typography>
        <div style={{
            paddingTop: '24px'
        }}>
            {isFetching ? (
                <Info>Загрузка...</Info>
            ) : error ? (
                <Info>ПРОИЗОШЛА ОШИБКА</Info>
            ) : (
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}>
                    {children}
                    <Pagination handle={setPagination} amountCards={length} />
                </div>
            )}
        </div>
    </Wrap>
)

