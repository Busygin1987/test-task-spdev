import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Wrapper from 'components/Wrapper';
import { Typography } from 'components/Typography';

const WrapLinks = styled.div`
    display: flex;
    height: 60px;
    width: 100%;
    justify-content: center;
    margin-top: 24px;
`;

const StyledLink = styled(Link)`
    margin-left: 24px;
`;

export default () => (
    <Wrapper title={'Home Page'}>
        <WrapLinks>
            <Link to='/posts'>
                <Typography link>Posts</Typography>
            </Link>
            <StyledLink to='/todos'>
                <Typography link>Todos</Typography>
            </StyledLink>
        </WrapLinks>
    </Wrapper>
);