import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from 'routes/Home';
import Login from 'routes/Login';
import Todos from 'routes/Todos';
import TodoItem from 'routes/TodoItem';
import Post from 'routes/Posts';
import PostItem from 'routes/PostItem';
import Forbidden from 'routes/Forbidden';
import NotFound from 'routes/NotFound';
import Header from 'modules/Header';
import PrivateRoute from 'modules/PrivateRoute';

function App() {
    return (
        <Switch>
            <Route path='/'>
                <Header />
                <Switch>

                    <Route exact path="/">
                        <Home />
                    </Route>

                    <Route exact path='/login'>
                        <Login />
                    </Route>

                    <Route exact path='/todos'>
                        <Todos />
                    </Route>

                    <Route exact path='/todos/:id'>
                        <TodoItem />
                    </Route>

                    <PrivateRoute
                        exact
                        path='/posts'
                        component={() => <Post />}
                    />

                    <Route exact path='/posts/:id'>
                        <PostItem />
                    </Route>

                    <Route exact path='/forbidden'>
                        <Forbidden />
                    </Route>

                    <Route path='/'>
                        <NotFound />
                    </Route>
            
                </Switch>
            </Route>
        </Switch>
    );
}

export default App;
