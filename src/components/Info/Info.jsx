import styled from 'styled-components';

export default styled.div`
    align-self: center;
    width: 100%;
    text-align: center;
    height: 48px;
    font-size: 20px;
    font-weight: bold;
    padding-top: 24px;
`;