export const actionTypes = {
    LOGIN: 'LOGIN',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
	LOGIN_FAILURE: 'LOGIN_FAILURE',
	LOGOUT: 'LOGOUT'
};

export const loginSuccess = (data = {}) => (dispatch) => {
	dispatch({
		type: actionTypes.LOGIN_SUCCESS,
		payload: data
	});
};

export const loginFailure = (data = '') => (dispatch) => {
	dispatch({
		type: actionTypes.LOGIN_FAILURE,
		payload: data
	});
};

export const login = () => (dispatch) => {
	dispatch({
		type: actionTypes.LOGIN
	});
};

export const logout = () => (dispatch) => {
	dispatch({
		type: actionTypes.LOGOUT
	});
};
