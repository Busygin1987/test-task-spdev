import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import WrapperPage from 'modules/WrapperPage';
import TodosCard from './TodosCard';

import {
    fetchTodosDataSuccess,
    fetchTodosDataFailure,
    fetchTodosData,
    setPaginationData
} from 'actions/todos';
import getData from 'utils/getData.js';

const Todos = React.memo(({
    todos,
    fetchTodos,
    fetchTodosSuccess,
    fetchTodosFailure,
    isFetching,
    setPagination,
    error,
    length
}) => {

    React.useEffect(() => {
        (() => {
            fetchTodos();
            getData({
                success: fetchTodosSuccess,
                fail: fetchTodosFailure,
                pagination: setPagination,
                type: 'todos'
            });
          })();
      }, []);

    return (
        <WrapperPage
            title={'Todos'}
            length={length}
            isFetching={isFetching}
            setPagination={setPagination}
            error={error}
        >
            {todos.map(({ title, completed, id }) => {
                return <TodosCard
                    title={title}
                    completed={completed}
                    key={id}
                    id={id}
                />
            })}
        </WrapperPage>
    )
});

const mapStateToProps = (state) => ({
    todos: state.todos.paginationData,
    isFetching: state.todos.isFetching,
    error: state.todos.hasError,
    length: state.todos.data.length
});

const mapActionsToProps = (dispatch) => ({
    fetchTodos: bindActionCreators(fetchTodosData, dispatch),
    fetchTodosSuccess: bindActionCreators(fetchTodosDataSuccess, dispatch),
    fetchTodosFailure: bindActionCreators(fetchTodosDataFailure, dispatch),
    setPagination: bindActionCreators(setPaginationData, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(Todos);