import React from 'react';
import styled from 'styled-components';

const AMOUNT_CARDS_ON_PAGE = 10;

const WrapPagination = styled.div`
    width: 550px;
    height: 30px;
    text-align: center;
`;

const Page = styled.span`
    width: 15px;
    height: 15px;
    display: inline-block;
    background-color: #d4cfcf;
    border: solid 1px ${(p) => p.active ? '#000' : '#7a7777'};
    margin: 0 5px;
    font-size: 14px;
    cursor: pointer;
`;

export default ({ handle, amountCards = 0 }) => {
    const [active, setActive] = React.useState(0);

    let renderPageNumbers;
    const pageNumbers = [];
    if (amountCards !== 0) {
        for (let i = 0; i < Math.ceil(amountCards / AMOUNT_CARDS_ON_PAGE); i++) {
            pageNumbers.push(i);
        }
        renderPageNumbers = pageNumbers.map(number => {
            return (
                <Page
                    key={number}
                    active={active === number}
                    onClick={() => {
                         if (active !== number) {
                            handle(number);
                            setActive(number);
                         }
                    }}
                >
                    {number + 1}
                </Page>
            );
        });
    }
    return (
        <WrapPagination>
            {renderPageNumbers && renderPageNumbers}
        </WrapPagination>
    )
};