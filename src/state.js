import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import todosReducer from 'reducers/todosReducer';
import authReducer from './reducers/authReducer';
import postsReducer from './reducers/postsReducer';

export default combineReducers({
    todos: todosReducer,
    form: formReducer,
    auth: authReducer,
    posts: postsReducer
});