import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';

import ContactForm from './ContactForm';
import {
    loginSuccess,
    loginFailure,
    login
} from 'actions/auth';

const Info = styled.div`
    align-self: center;
    width: 100%;
    text-align: center;
    height: 48px;
    font-size: 20px;
    font-weight: bold;
    padding-top: 24px;
`;

const WrapForm = styled.div`
    margin: 24px 0 0 24px;
    padding: 24px;
    border: solid 1px #000;
    width: 300px;
`;

const Login = ({
    users,
    isChecking,
    error,
    login,
    loginSuccess,
    loginFailure,
    history
}) => {

    const handleSubmit = (values) => {
        login();
        users.map((user) => {
            if (user.login === values.login && user.password === Number(values.password)) {
                loginSuccess({ name: user.userName, role: user.userRole });
                history.push('/');
            } else {
                loginFailure('Неверный логин или пароль!');
            }
        });
    }

    return (
        <>
            {isChecking ? (
                    <Info>Проверка...</Info>
                ) : (
                    <WrapForm>
                        <ContactForm onSubmit={handleSubmit} />
                        {error && <Info>{error}</Info>}
                    </WrapForm>
                )
            }
        </>
    )
}

const mapStateToProps = (state) => ({
    isChecking: state.auth.isChecking,
    error: state.auth.hasError,
    users: state.auth.users
});

const mapActionsToProps = (dispatch) => ({
    login: bindActionCreators(login, dispatch),
    loginSuccess: bindActionCreators(loginSuccess, dispatch),
    loginFailure: bindActionCreators(loginFailure, dispatch),
});

export default withRouter(connect(mapStateToProps, mapActionsToProps)(Login));