import { actionTypes } from 'actions/auth';

const initialState = {
	users: [
		{
			userName: 'User Name 1',
			login: 'user1',
			userRole: 'USER',
			password: 12345
		},
		{
			userName: 'User Name 2',
			login: 'user2',
			userRole: 'USER',
			password: 12345
		},
		{
			userName: 'Admin',
			login: 'admin',
			userRole: 'ADMIN',
			password: 12345
		},
	],
	currentUser: {},
	isChecking: false,
    hasError: '',
};

export default (state = initialState, action) => {
	Object.freeze(state);
	switch (action.type) {
		case actionTypes.LOGIN:
			return {
				...state,
				isChecking: true,
			};
		case actionTypes.LOGIN_SUCCESS:
			localStorage.setItem("name", action.payload.name);
      		localStorage.setItem("role", action.payload.role);
			return {
				...state,
				hasError: '',
				isChecking: false,
				currentUser: action.payload,
			};
		case actionTypes.LOGIN_FAILURE:
			return {
				...state,
				hasError: action.payload,
				isChecking: false,
			};
		case actionTypes.LOGOUT:
			localStorage.clear();
			return {
				...state,
				hasError: '',
				isChecking: false,
				currentUser: {}
			};
		default:
			return state;
	}
}