export const actionTypes = {
    FETCH_TODOS: 'FETCH_TODOS',
    FETCH_TODOS_SUCCESS: 'FETCH_TODOS_SUCCESS',
	FETCH_TODOS_FAILURE: 'FETCH_TODOS_FAILURE',
	SET_TODOS_PAGINATION: 'SET_TODOS_PAGINATION',
	FETCH_TODO_ITEM: 'FETCH_TODO_ITEM',
    FETCH_TODO_ITEM_SUCCESS: 'FETCH_TODO_ITEM_SUCCESS',
	FETCH_TODO_ITEM_FAILURE: 'FETCH_TODO_ITEM_FAILURE',
	CLEAR_TODO_ITEM: 'CLEAR_TODO_ITEM',
};

export const fetchTodosDataSuccess = (data = []) => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_TODOS_SUCCESS,
		payload: data
	});
};

export const fetchTodosDataFailure = () => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_TODOS_FAILURE
	});
};

export const fetchTodosData = () => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_TODOS
	});
};

export const setPaginationData = (data) => (dispatch) => {
	dispatch({
		type: actionTypes.SET_TODOS_PAGINATION,
		payload: data
	});
};

export const fetchTodoDataSuccess = (data = []) => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_TODO_ITEM_SUCCESS,
		payload: data
	});
};

export const fetchTodoDataFailure = () => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_TODO_ITEM_FAILURE
	});
};

export const fetchTodoData = () => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_TODO_ITEM
	});
};

export const clearTodoData = () => (dispatch) => {
	dispatch({
		type: actionTypes.CLEAR_TODO_ITEM
	});
};