import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import App from './App';
import AuthProvider from 'services/Auth';
import GlobalStyles from './globalStyles.js';
import rootReducer from './state';

const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <AuthProvider>
                <App />
                <GlobalStyles/>
            </AuthProvider>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);