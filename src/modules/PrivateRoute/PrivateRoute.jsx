import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      localStorage.role === 'ADMIN'
        ? <Component {...props} />
        :  <Redirect to="/forbidden" />
    )}
  />
);
export default PrivateRoute;