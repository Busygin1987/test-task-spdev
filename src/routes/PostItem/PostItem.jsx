import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import Wrapper from 'components/Wrapper';
import Info from 'components/Info';
import { Typography } from 'components/Typography';
import {
    fetchPostDataSuccess,
    fetchPostDataFailure,
    fetchPostData,
    clearPostData
} from 'actions/posts';
import getData from 'utils/getData.js';

const PostItem = ({
    post: {
        title,
        body
    },
    history,
    fetchPostSuccess,
    fetchPostFailure,
    fetchPost,
    clearPost,
    isFetching,
    error
}) => {

    React.useEffect(() => {
        const id = history.location.pathname.split('/')[2];
        (() => {
            fetchPost();
            getData({ success: fetchPostSuccess, fail: fetchPostFailure, id, type: 'posts' });
          })();
    }, []);

    React.useEffect(() => () => clearPost(), []);

    return (
        isFetching ? (
            <Info>Загрузка...</Info>
        ) : error ? (
            <Info>ПРОИЗОШЛА ОШИБКА</Info>
        ) : (
            <Wrapper title={title}>
                <div style={{
                    width: '500px',
                    height: 'fit-content',
                    border: 'solid 1px #000',
                    marginTop: '24px',
                    padding: '24px'
                }}>
                    <Typography>
                        <strong>Description: </strong>{body}
                    </Typography>
                </div>
            </Wrapper>
        )
    )
};

const mapStateToProps = (state) => ({
    post: state.posts.itemData,
    isFetching: state.posts.isFetching,
    error: state.posts.hasError,
});

const mapActionsToProps = (dispatch) => ({
    fetchPost: bindActionCreators(fetchPostData, dispatch),
    fetchPostSuccess: bindActionCreators(fetchPostDataSuccess, dispatch),
    fetchPostFailure: bindActionCreators(fetchPostDataFailure, dispatch),
    clearPost: bindActionCreators(clearPostData, dispatch),
});

export default connect(mapStateToProps, mapActionsToProps)(withRouter(PostItem));