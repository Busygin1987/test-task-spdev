import styled from 'styled-components';

export default styled.span`
    font-weight: ${({ fw }) => fw || 'normal'};
    font-size: ${({ fs }) => fs ? fs + 'px' : '24px'};
    display: inline-block;
    color: ${(p) => p.link ? '#0553b5' : p.color ? p.color : '#000'};
`;