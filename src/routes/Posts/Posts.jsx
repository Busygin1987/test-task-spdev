import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import WrapperPage from 'modules/WrapperPage';
import PostsCard from './PostsCard';

import {
    fetchPostsDataSuccess,
    fetchPostsDataFailure,
    fetchPostsData,
    setPaginationData
} from 'actions/posts';
import getData from 'utils/getData.js';

const Posts = React.memo(({
    posts,
    fetchPosts,
    fetchPostsSuccess,
    fetchPostsFailure,
    isFetching,
    setPagination,
    error,
    length
}) => {

    React.useEffect(() => {
        (() => {
            fetchPosts();
            getData({ success: fetchPostsSuccess, fail: fetchPostsFailure, pagination: setPagination, type: 'posts' });
          })();
      }, []);

    return (
        <WrapperPage
            title={'Posts'}
            length={length}
            isFetching={isFetching}
            setPagination={setPagination}
            error={error}
        >
            {posts.map((item) => {
                return <PostsCard
                    title={item.title}
                    body={item.body}
                    key={item.id}
                    id={item.id}
                />
            })}
        </WrapperPage>
    )
});

const mapStateToProps = (state) => ({
    posts: state.posts.paginationData,
    isFetching: state.posts.isFetching,
    error: state.posts.hasError,
    length: state.posts.data.length
});

const mapActionsToProps = (dispatch) => ({
    fetchPosts: bindActionCreators(fetchPostsData, dispatch),
    fetchPostsSuccess: bindActionCreators(fetchPostsDataSuccess, dispatch),
    fetchPostsFailure: bindActionCreators(fetchPostsDataFailure, dispatch),
    setPagination: bindActionCreators(setPaginationData, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(Posts);