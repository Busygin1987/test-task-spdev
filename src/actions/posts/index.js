export const actionTypes = {
    FETCH_POSTS: 'FETCH_POSTS',
    FETCH_POSTS_SUCCESS: 'FETCH_POSTS_SUCCESS',
	FETCH_POSTS_FAILURE: 'FETCH_POSTS_FAILURE',
	SET_POSTS_PAGINATION: 'SET_POSTS_PAGINATION',
	FETCH_POST_ITEM: 'FETCH_POST_ITEM',
    FETCH_POST_ITEM_SUCCESS: 'FETCH_POST_ITEM_SUCCESS',
	FETCH_POST_ITEM_FAILURE: 'FETCH_POST_ITEM_FAILURE',
	CLEAR_POST_ITEM: 'CLEAR_POST_ITEM',
};

export const fetchPostsDataSuccess = (data = []) => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_POSTS_SUCCESS,
		payload: data
	});
};

export const fetchPostsDataFailure = () => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_POSTS_FAILURE
	});
};

export const fetchPostsData = () => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_POSTS
	});
};

export const setPaginationData = (data) => (dispatch) => {
	dispatch({
		type: actionTypes.SET_POSTS_PAGINATION,
		payload: data
	});
};

export const fetchPostDataSuccess = (data = []) => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_POST_ITEM_SUCCESS,
		payload: data
	});
};

export const fetchPostDataFailure = () => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_POST_ITEM_FAILURE
	});
};

export const fetchPostData = () => (dispatch) => {
	dispatch({
		type: actionTypes.FETCH_POST_ITEM
	});
};

export const clearPostData = () => (dispatch) => {
	dispatch({
		type: actionTypes.CLEAR_POST_ITEM
	});
};