import { actionTypes } from 'actions/posts';

const initialState = {
    isFetching: false,
    data: [],
    hasError: false,
    paginationData: [],
    itemData: {},
}

export default (state = initialState, action) => {
    Object.freeze(state);
	switch (action.type) {
        case actionTypes.FETCH_POSTS:
            return {
                ...state,
                isFetching: true,
            };
        case actionTypes.FETCH_POSTS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                data: action.payload,
            };
        case actionTypes.FETCH_POSTS_FAILURE:
            return {
                ...state,
                hasError: true,
                isFetching: false,
            };
        case actionTypes.SET_POSTS_PAGINATION:
            const data = state.data.slice(action.payload * 10, (action.payload * 10 + 10));
            return {
                ...state,
                paginationData: data,
            };
        case actionTypes.FETCH_POST_ITEM:
            return {
                ...state,
                isFetching: true,
            };
        case actionTypes.FETCH_POST_ITEM_SUCCESS:
            return {
                ...state,
                isFetching: false,
                itemData: action.payload,
            };
        case actionTypes.FETCH_POST_ITEM_FAILURE:
            return {
                ...state,
                hasError: true,
                isFetching: false,
            };
        case actionTypes.CLEAR_POST_ITEM:
            return {
                ...state,
                hasError: false,
                itemData: {}
            };
		default:
			return state;
	}
}