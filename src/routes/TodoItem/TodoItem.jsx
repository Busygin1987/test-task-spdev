import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import Wrapper from 'components/Wrapper';
import Info from 'components/Info';
import { Typography } from 'components/Typography';
import {
    fetchTodoDataSuccess,
    fetchTodoDataFailure,
    fetchTodoData,
    clearTodoData
} from 'actions/todos';
import getData from 'utils/getData.js';

const TodoItem = ({
    todo: {
        title
    },
    history,
    fetchTodoSuccess,
    fetchTodoFailure,
    fetchTodo,
    clearTodo,
    isFetching,
    error
}) => {

    React.useEffect(() => {
        const id = history.location.pathname.split('/')[2];
        (() => {
            fetchTodo();
            getData({ success: fetchTodoSuccess, fail: fetchTodoFailure, id, type: 'todos' });
          })();
    }, []);

    React.useEffect(() => () => clearTodo(), []);

    return (
        isFetching ? (
            <Info>Загрузка...</Info>
        ) : error ? (
            <Info>ПРОИЗОШЛА ОШИБКА</Info>
        ) : (
            <Wrapper title={title}>
                <div style={{
                    width: '500px',
                    height: 'fit-content',
                    border: 'solid 1px #000',
                    marginTop: '24px',
                    padding: '24px'
                }}>
                    <Typography>
                        {'Description: '}
                    </Typography>
                </div>
            </Wrapper>
        )
    )
};

const mapStateToProps = (state) => ({
    todo: state.todos.itemData,
    isFetching: state.todos.isFetching,
    error: state.todos.hasError,
});

const mapActionsToProps = (dispatch) => ({
    fetchTodo: bindActionCreators(fetchTodoData, dispatch),
    fetchTodoSuccess: bindActionCreators(fetchTodoDataSuccess, dispatch),
    fetchTodoFailure: bindActionCreators(fetchTodoDataFailure, dispatch),
    clearTodo: bindActionCreators(clearTodoData, dispatch),
});

export default connect(mapStateToProps, mapActionsToProps)(withRouter(TodoItem));