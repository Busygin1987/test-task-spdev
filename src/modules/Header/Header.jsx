import React from 'react';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Typography } from 'components/Typography';
import { Button } from 'components/Button';
import { logout } from 'actions/auth';

const WrapHeader = styled.div`
    width: 100%;
    height: 80px;
    box-sizing: border-box;
    background-color: #f5f5f5;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 24px;
`;

const WrapLogo = styled.div`
     cursor: pointer;
`;

const Header = ({ currentUser, logout, history }) => (
    <WrapHeader>
          <WrapLogo
               onClick={() => {
                    history.push('/');
               }}
          >
               <Typography fw={'bold'} fs={40}>{'LOGO'}</Typography>
          </WrapLogo>
          <div style={{
               width: '270px',
               textAlign: 'end'
          }}>
               <span style={{
                    paddingRight: '24px'
               }}
               >
                    <Typography fs={24}>{localStorage.name || currentUser.name}</Typography>
               </span>
               <Button
                    onClick={() => {
                         if (localStorage.name || currentUser.name) {
                              logout();
                         } else {
                              history.push('/login');
                         }
                    }}
               >
                    {localStorage.name || currentUser.name ? 'Logout' : 'Login'}
               </Button>
          </div>
    </WrapHeader>
);

const mapStateToProps = (state) => ({
     currentUser: state.auth.currentUser
 });
 
 const mapActionsToProps = (dispatch) => ({
     logout: bindActionCreators(logout, dispatch),
 });
 
 export default withRouter(connect(mapStateToProps, mapActionsToProps)(Header));