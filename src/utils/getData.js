import axios from 'axios';

export default async function ({
    success,
    fail,
    pagination = () => {},
    id = '',
    type
}) {
    try {
        const res = await axios.get(`http://jsonplaceholder.typicode.com/${type}/${id}`);
        if (res.data) {
            success(res.data);
            pagination(0);
        }
    } catch (error) {
        console.error(error)
        fail();
    }
}