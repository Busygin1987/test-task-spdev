import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0; 
    }
    
	body {
		box-sizing: border-box;
		margin: 0;
		padding: 0;
		width: 100%;
		height: 100%;
		font-family: Georgia, 'Times New Roman', Times, serif;;
	}

	#root {
		user-select: none;
		height: 100%;
		width: 100%;
	}
`;
