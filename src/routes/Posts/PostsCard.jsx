import React from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

import { Typography } from 'components/Typography';

const Card = styled.div`
    width: 400px;
    min-height: 80px;
    border: solid 1px #0f468c;
    border-radius: 10px;
    padding: 12px;
    box-sizing: border-box;
    margin-bottom: 12px;
    cursor: pointer;
`;

export default withRouter(({ title, body, id, history }) => (
    <Card
        onClick={() => {
            history.push(`/posts/${id}`);
        }}
    >
        <Typography color={'#0f468c'} fs={18}><strong>Title:</strong> {title}</Typography><br />
        <Typography color={'#0f468c'} fs={16}><strong>Description:</strong> {body}</Typography>
    </Card>
));