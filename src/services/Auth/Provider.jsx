import React from 'react';
import { withRouter } from 'react-router-dom';

export default withRouter(({ children, history }) => {

    React.useEffect(() => {
        if (!localStorage.getItem('role')) {
            history.push('/login');
        } 
    }, []);

    return (
        <React.Fragment>
            {children}
        </React.Fragment>
    )
});