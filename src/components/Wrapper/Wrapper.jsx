import React from 'react';
import styled from 'styled-components';

import { Typography } from 'components/Typography';

const Wrap = styled.div`
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    background-color: #9e9b9b;
    display: flex;
    align-items: center;
    flex-direction: column;
    padding: 24px;
`;

export default ({ 
    title,
    children,
    ...props
}) => (
    <Wrap>
        <Typography props={props}>
            <div style={{
                width: '300px',
                minHeight: '50px',
                textAlign: 'center'
            }}>
                {title}
            </div>
        </Typography>
        {children}
    </Wrap>
)

